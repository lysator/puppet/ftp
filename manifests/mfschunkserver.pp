#Installerar konfiguration för moosefs chunkservrar.
#
# Den här klassen kräver för tillfället att man manuellt installerar
# mfschunkserver då den inte finns packeterad för OpenSUSE.
#
class ftp::mfschunkserver {
    ensure_packages([
      'node_exporter',
    ])

    file { '/usr/local/etc/mfs/mfschunkserver.cfg':
        ensure  => file,
        content => epp('ftp/mfschunkserver.epp'),
        owner   => 'root',
        group   => 'wheel',
        mode    => '0644',
    }

    systemd::unit_file {'mfschunkserver.service':
        source => 'puppet:///modules/ftp/mfschunkserver/etc_mfs/mfschunkserver.service',
    }
    ~> service {'mfschunkserver':
        ensure => running,
    }

    service {'prometheus-node_exporter':
        ensure => running,
    }
}
