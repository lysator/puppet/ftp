#
class ftp::frontend
{
  ensure_packages([
    'curl',
    'git',
    'haproxy',
    'ipmitool',
    'keepalived',
    'node_exporter',
    'openntpd',
    'puppet',
    'smartmontools',
    'socat',
    'sshguard',
    'tmux',
    'varnish',
    'vim',
    'vnstat',
    'xtools',
  ])

  file { '/etc/haproxy/haproxy.cfg':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/ftp/frontend/haproxy/haproxy.cfg',
  }

  file { '/etc/haproxy/botlista.txt':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/ftp/frontend/haproxy/botlista.txt',
  }

  file { '/etc/haproxy/cachelista.txt':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/ftp/frontend/haproxy/cachelista.txt',
  }

  file { '/etc/haproxy/mirror_backends.txt':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/ftp/frontend/haproxy/mirror_backends.txt',
  }

  service { 'haproxy':
    provider => 'runit',
    enable   => true,
    restart  => 'sv reload haproxy',
  }

  file { '/etc/varnish/default.vcl':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/ftp/frontend/varnish/default.vcl',
  }

  file { '/etc/sv/varnishd/conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/ftp/frontend/varnish/conf-${facts['hostname']}",
  }

  service { 'varnishd':
    provider => 'runit',
    enable   => true,
  }

  file { '/etc/keepalived/keepalived.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/ftp/frontend/keepalived/keepalived.conf-${facts['hostname']}",
  }

  service { 'keepalived':
    provider => 'runit',
    enable   => true,
  }
}

