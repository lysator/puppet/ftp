# Installerar konfiguration för MooseFS. Då Anzu som är mästarserver
# för tillfället kör Gentoo med USE-flaggor, overlay och lite annat
# för att få MooseFS installerat så får man se till att det är
# installerat manuellt innan man applicerar denna klass.
class ftp::mfsmaster {
  file { '/etc/init.d/mfsmaster':
    ensure => file,
    source => 'puppet:///modules/ftp/mfsmaster/init.d_mfsmaster',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  service { 'mfsmaster':
    ensure  => running,
    enable  => true,
    require => File['/etc/init.d/mfsmaster'],
  }

  file { '/etc/mfs/mfsmaster.cfg':
    ensure => file,
    source => 'puppet:///modules/ftp/mfsmaster/etc_mfs/mfsmaster.cfg',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file { '/etc/mfs/mfsexports.cfg':
    ensure => file,
    source => 'puppet:///modules/ftp/mfsmaster/etc_mfs/mfsexports.cfg',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['reload_mfsmaster'],
  }

  file { '/etc/mfs/mfstopology.cfg':
    ensure => file,
    source => 'puppet:///modules/ftp/mfsmaster/etc_mfs/mfstopology.cfg',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file { '/etc/hosts':
    ensure => file,
    source => 'puppet:///modules/ftp/mfsmaster/etc_hosts',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  exec { 'reload_mfsmaster':
    command     => '/usr/sbin/mfsmaster reload',
    refreshonly => true,
  }
}
