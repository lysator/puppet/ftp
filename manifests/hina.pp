class ftp::hina {
  ensure_packages([
    'curl',
    'jq',
    'moreutils',
    'nginx',
    'nginx-prometheus-exporter',
    'node_exporter',
    'py39-certbot-nginx',
    'rsync',
    'rsyslog',
    'tor',
    'pure-ftpd',
  ])

  file { '/usr/local/etc/nginx/nginx.conf':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/nginx.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/tor/torrc':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/torrc',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/bin/acme_sync.sh':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/acme_sync.sh',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0555',
  }

  file { '/usr/local/bin/git_mirror.sh':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/git_mirror.sh',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0555',
  }

  file { '/etc/cron.d/ftp-sync':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/ftp-sync',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/rsync/rsyncd.conf':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/rsyncd.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0444',
  }

  file { '/usr/local/etc/rsyncd.conf':
    ensure => link,
    target => '/usr/local/etc/rsync/rsyncd.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0444',
  }

  file { '/usr/local/etc/rsync_motd':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/rsync_motd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { 'newsyslog.conf.d':
    ensure  => file,
    path    => '/usr/local/etc/newsyslog.conf.d',
    source  => 'puppet:///modules/ftp/hina/newsyslog.conf.d/',
    recurse => remote,
    purge   => false,
  }

  file { '/usr/local/etc/rsyslog.conf':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/rsyslog.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  user { 'rsyslogd-user':
    ensure => present,
    name   => 'rsyslogd',
    home   => '/var/empty',
    shell  => '/usr/sbin/nologin',
  }

  file_line { 'start rsyslogd':
    path => '/etc/rc.local',
    line => 'mkdir -p /var/run/rsyslogd && chown rsyslogd:rsyslogd /var/run/rsyslogd && daemon -u rsyslogd rsyslogd -i /var/run/rsyslogd/rsyslogd-haproxy.pid',
  }

  file { '/var/log/haproxy.log':
    ensure  => present,
    content => '',
    replace => 'no',
    owner   => 'rsyslogd',
    group   => 'rsyslogd',
    mode    => '0644',
  }

  file { 'ftprsync':
    ensure  => file,
    path    => '/usr/local/bin',
    source  => 'puppet:///modules/ftp/hina/ftprsync/',
    recurse => remote,
    purge   => false,
  }

  file { 'ftprsync-config':
    ensure  => file,
    purge   => false,
    path    => '/usr/local/etc/ftprsync',
    source  => 'puppet:///modules/ftp/hina/ftprsync-config/',
    recurse => remote,
  }

  file { '/usr/local/etc/pure-ftpd.conf':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/pure-ftpd.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/pure-ftpd-motd':
    ensure => file,
    source => 'puppet:///modules/ftp/hina/pure-ftpd-motd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  service { 'pure-ftpd':
    ensure => running,
    enable => true,
  }
}
