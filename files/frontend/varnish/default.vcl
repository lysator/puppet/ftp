vcl 4.1;


acl purge {
        "localhost";
	# Hina
        "130.236.254.195";
        "2001:6b0:17:f0a0::c3";
	# FTP-push
        "130.236.254.138";
        "2001:6b0:17:f0a0::8a";
	# Infiniband-nät
        "10.42.0.0/16";
}

backend dataserver {
        .host = "localhost";
        .port = "1447";
# Varnish suger (data för snabbt så sonden misslyckas)
#        .probe = {
#                .url = "/";
#                .timeout = 1s;
#                .interval = 5s;
#                .window = 5;
#                .threshold = 3;
#        }
}

sub vcl_recv {
        if (req.method == "PURGE")
        {
                if (!client.ip ~ purge)
                {
                        return(synth(405, "Not allowed."));
                }
                return (purge);
        }
        if (req.method == "HEAD")
        {
                return(pass);
        }
	if (req.url ~ "-repodata$") {
                return(pass);
        }
        if (req.url ~ "/repodata/") {
                return(pass);
        }
        if (req.url ~ "Packages.(gz|bz2)$") {
                return(pass);
        }
        if (req.url ~ "/pub/(archlinux|manjaro)/.*\.(db|db.tar.gz|extra|extra.tar.gz)$") {
                return(pass);
        }
}

sub vcl_backend_response {
	if (beresp.status == 404) {
		set beresp.uncacheable = true;
		return(deliver);
	}

	set beresp.ttl = 120h;
}

sub vcl_deliver {
        unset resp.http.Age;
        unset resp.http.X-Varnish;
        unset resp.http.Via;
}

sub vcl_hash {
        hash_data(req.url);

        return (lookup);
}

