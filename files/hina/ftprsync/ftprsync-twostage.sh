#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

LOCKFILEDIR=/var/run/ftprsync/
LOCALREPODIR=/var/ftp/pub/
LOGFILEDIR=/var/log/ftprsync/
LOCKFILE=
RSYNCPROG=/usr/local/bin/rsync
RSYNCCMD1="-vrlt --hard-links --stats --progress --exclude 'Packages*' --exclude 'Sources*' --exclude 'Release*' --exclude 'InRelease' --exclude 'ls-lR*' --chmod ug+w,o-w,+rX"
RSYNCCMD2="-vrlt --hard-links --delete --delete-after --delay-updates --stats --progress --chmod ug+w,o-w,+rX"
RSYNCSERV=
RSYNCREPO=
LOCALREPO=
LOGFILE=
RSYNCEXIT=

if [ $# -ne 5 ]
then
    echo usage: $0 rsync-server rsync-repo local-repo lockfile logfile >&2
    exit 1
fi

RSYNCSERV=$1
RSYNCREPO=$2
LOCALREPO=$LOCALREPODIR$3
LOCKFILE=$LOCKFILEDIR$4
LOGFILEPREFIX=$5
LOGFILE=$LOGFILEDIR$LOGFILEPREFIX.`date +%Y-%m-%d_%H:%M:%S`.out

if [ ! -d "$LOGFILEDIR" ]
then
	mkdir -p "$LOGFILEDIR"
fi

if [ ! -d "$LOCKFILEDIR" ]
then
	mkdir -p "$LOCKFILEDIR"
fi

if [ -e $LOCKFILE ]
then
	echo "lockfile exists, rsync already running (syncing $LOCALREPO)" >&2
	exit 2
fi

cp /dev/null $LOGFILE
if [ $? != 0 ]
then
	echo "cannot create logfile, check permissions (syncing $LOCALREPO)" >&2
	exit 3
fi

cp /dev/null $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot create lockfile, check permissions (syncing $LOCALREPO)" >&2
	exit 4
fi

for file in `find $LOGFILEDIR -name "$LOGFILEPREFIX.*" | sort -n -r | tail +9`
do
	rm $file
done


echo "starting first stage of sync" > $LOGFILE
echo "$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD1 $RSYNCSERV::$RSYNCREPO $LOCALREPO/" >> $LOGFILE
for i in 1 2 3
do
	$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD1 $RSYNCSERV::$RSYNCREPO $LOCALREPO/ > /dev/null 2>&1
	RSYNCEXIT=$?
	if [ $RSYNCEXIT -eq 0 ]
	then
		echo "starting second stage of sync" >> $LOGFILE
		echo "$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD2 $RSYNCSERV::$RSYNCREPO $LOCALREPO/" >> $LOGFILE
		for j in 1 2 3
		do
			$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD2 $RSYNCSERV::$RSYNCREPO $LOCALREPO/ > /dev/null 2>&1
			RSYNCEXIT=$?
			if [ $RSYNCEXIT -eq 0 ]
			then
				break
			else
				echo "rsync failed in second stage when syncing $LOCALREPO, exit code was $RSYNCEXIT (attemt $j)" >&2
                		echo "last lines of $LOGFILE is" >&2
                		echo "======================================="
                		tail -n 16 $LOGFILE >&2
                		echo "======================================="
                		echo ""
				echo "rsync failed in second stage, exit code was $RSYNCEXIT (attemt $j)" >> $LOGFILE
			fi
		done
		break
	else
		echo "rsync failed in first stage when syncing $LOCALREPO, exit code was $RSYNCEXIT (attempt $i)" >&2
                echo "last lines of $LOGFILE is" >&2
                echo "======================================="
                tail -n 16 $LOGFILE >&2
                echo "======================================="
                echo ""
		echo "rsync failed in first stage, exit code was $RSYNCEXIT (attempt $i)" >> $LOGFILE
	fi
done

/usr/local/bin/cache-flush.py $LOGFILE $LOCALREPO

rm $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot remove lockfile, remove it by hand (syncing $LOCALREPO)" >&2
fi

if [ $RSYNCEXIT != 0 ]
then
	echo rsync failed after three attempts when syncing $LOCALREPO >&2
	exit 5
else
	date -u > $LOCALREPO/project/trace/ftp.lysator.liu.se
	exit 0
fi
